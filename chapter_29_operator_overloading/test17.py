class Number:
    def __init__(self, val):
        self.val = val

    def __add__(self, other):
        return Number(self.val + other)


x = Number(5)
x += 1
x += 1
print(x.val)
