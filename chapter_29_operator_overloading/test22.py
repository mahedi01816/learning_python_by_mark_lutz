from tkinter import Button


class Callback:
    def __init__(self, color):
        self.color = color

    def changeColor(self):
        print('turn', self)


cb1 = Callback('blue')
cb2 = Callback('green')

B1 = Button(command=cb1.changeColor)
B2 = Button(command=cb2.changeColor)

object = Callback('blue')
cb = object.changeColor
print(cb())
