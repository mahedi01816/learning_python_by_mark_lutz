class Printer:
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return str(self.val)


objs = [Printer(3), Printer(2)]
for x in objs:
    print(x)

print(objs)
