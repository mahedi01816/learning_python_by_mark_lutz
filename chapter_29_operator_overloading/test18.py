class Callee:
    def __call__(self, *pargs, **kargs):
        print('Called : ', pargs, kargs)


C = Callee()
C(1, 2, 3)
C(1, 2, 3, x=4, y=5)
