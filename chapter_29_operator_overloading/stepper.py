class Stepper:
    def __getitem__(self, i):
        return self.data[i]


X = Stepper()
X.data = 'spam'

print(X[1])

for item in X:
    print(item, end=' ')

print('\n', 'p' in X)
print([c for c in X])
print(list(map(str.upper, X)))

(a, b, c, d) = X
print(a, c, d)
print(list(X))
print(tuple(X))
print(''.join(X))
print(X)