class Prod:
    def __init__(self, value):
        self.value = value

    def comp(self, other):
        return self.value * other


x = Prod(3)
print(x.comp(3))
print(x.comp(4))
