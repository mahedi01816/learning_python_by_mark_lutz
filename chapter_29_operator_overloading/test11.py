class Adder:
    def __init__(self, value=0):
        self.data = value

    def __add__(self, other):
        self.data += other


class Addrepr(Adder):
    def __repr__(self):
        return 'Addrepr %s' % self.data


class Addstr(Adder):
    def __str__(self):
        return '[Value : %s]' % self.data


class Addboth(Adder):
    def __str__(self):
        return '[Value : %s]' % self.data

    def __repr__(self):
        return 'Addboth(%s)' % self.data


y = Adder()
# print(y)
x = Addrepr(2)
# print(x)
x + 1
# print(x)
# print(str(x), repr(x))
z = Addstr(3)
# print(z)
# print(str(z), repr(z))
a = Addboth(4)
print(a)
print(str(a), repr(a))
