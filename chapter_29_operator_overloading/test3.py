def gsquares(start, stop):
    for i in range(start, stop + 1):
        yield i ** 2


for i in gsquares(1, 5):
    print(i, end=' ')
