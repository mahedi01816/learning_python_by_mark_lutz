class Truth:
    pass


class Falsy:
    pass


X = Truth()
print(bool(X))

Y = Falsy()
print(bool(Y))
