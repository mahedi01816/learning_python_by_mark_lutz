class Squares:
    def __init__(self, start, stop):
        self.value = start - 1
        self.stop = stop

    def __iter__(self):
        return self

    def __next__(self):
        if self.value == self.stop:
            raise StopIteration
        self.value += 1
        return self.value ** 2


for i in Squares(1, 5):
    print(i, end=' ')

X = Squares(1, 5)
I = iter(X)
print('\n', next(I))
print(next(I))
print(next(I))
# print(X[1])
print([n for n in X])
print([n for n in X])
print([n for n in Squares(1, 5)])
print(list(Squares(1, 3)))
