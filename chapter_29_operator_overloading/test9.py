class AccessControl:
    def __setattr__(self, attr, value):
        if attr == 'age':
            self.__dict__[attr] = value
        else:
            raise AttributeError(attr + ' not allowed !!!')


X = AccessControl()
X.age = 40
print(X.age)
X.name = 'mel'
