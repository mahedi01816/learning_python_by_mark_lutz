def func(a):
    b = 'Spam'
    return b * a


print(func(8))

print(func.__name__)
print(dir(func))
print(func.__code__)
print(dir(func.__code__))
print(func.__code__.co_varnames)
print(func.__code__.co_argcount)
print(func)
func.count = 0
func.count += 1
print(func.count)
func.handlers = 'Button-Press'
print(func.handlers)
print(dir(func))
