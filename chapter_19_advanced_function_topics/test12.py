def func(a: 'spam', b, c: 99):
    return a + b + c


print(func(1, 2, 3))
print(func.__annotations__)
for arg in func.__annotations__:
    print(arg, ' => ', func.__annotations__[arg])
