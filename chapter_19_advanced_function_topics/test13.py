def func(a: 'spam' = 4, b: (1, 10) = 5, c: float = 6):
    return a + b + c


print(func(1, 2, 3))
print(func())
print(func(1, c=10))
print(func.__annotations__)
