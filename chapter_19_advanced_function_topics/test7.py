def echo(message):
    print(message)


echo('Direct call')

x = echo
x('Indirect call')


def make(label):
    def echo(message):
        print(label, ' : ', message)

    return echo


F = make('Spam')
F('Ham!')
F('Eggs!')
