class Rec:
    pass


Rec.name = 'Bob'
Rec.age = 40

print(Rec.name)

x = Rec()
y = Rec()

print(x.name)
print(y.name)

x.name = 'Sue'
print(Rec.name, x.name, y.name)

print(Rec.__dict__.keys())
print(list(x.__dict__.keys()))
print(list(y.__dict__.keys()))

print(x.__class__)
print(Rec.__bases__)


def upperName(self):
    return self.name.upper()


print(upperName(x))

Rec.method = upperName
print(x.method(), y.method())

print(Rec.method(x))