def metaFunc(classname, supers, classdict):
    print('In metaFunc : ', classname, supers, classdict, sep='\n...')
    return type(classname, supers, classdict)


class Eggs:
    pass


print('making class')


class Spam(Eggs, metaclass=metaFunc):
    data = 1

    def meth(self, arg):
        pass


print('making instance')
X = Spam()
print('data : ', X.data)
