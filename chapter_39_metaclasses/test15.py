from types import FunctionType
from mytools import tracer, timer


def decorateall(decorator):
    def decoDecorate(aClass):
        for attr, attrval in aClass.__dict__.items():
            if type(attrval) is FunctionType:
                setattr(aClass, attr, decorator(attrval))
        return aClass

    return decoDecorate


@decorateall(tracer)
class Person:
    def __init__(self, name, pay):
        self.name = name
        self.pay = pay

    def giveRaise(self, percent):
        self.pay *= (1 + percent)

    def lastName(self):
        return self.name.split()[-1]


bob = Person('Bob Smith', 4000)
sue = Person('Sue Jones', 500)
