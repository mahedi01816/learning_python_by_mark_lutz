class Classic:
    def __getattr__(self, name):
        if name == 'age':
            return 40
        else:
            raise AttributeError


x = Classic()
print(x.age)
print(x.name)
