class C:
    shared = []

    def __init__(self):
        self.perobj = []


x = C()
y = C()

print(y.shared)
print(y.perobj)

x.shared.append('spam')
x.perobj.append('spam')
y.perobj.append('not!')

print(x.shared)
print(y.shared)
print(x.perobj)
print(y.perobj)
print(C.shared)