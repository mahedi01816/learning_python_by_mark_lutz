class A:
    def meth(s):
        print('A.meth')


class C(A):
    def meth(s):
        print('C.meth')


class B(A):
    pass


class D(B, C):
    pass


x = D()
x.meth()


class D(B, C):
    meth = C.meth


x = D()
x.meth()


class D(B, C):
    meth = B.meth


x = D()
x.meth()
