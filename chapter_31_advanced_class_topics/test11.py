class Spam:
    numInstances = 0

    def __init__(self):
        Spam.numInstances += 1

    def printNumInstances():
        print('Number of instances : ', Spam.numInstances)

    printNumInstances = staticmethod(printNumInstances)


class Sub(Spam):
    def printNumInstances():
        print('Extra stuff...')
        Spam.printNumInstances()

    printNumInstances = staticmethod(printNumInstances)


class Other(Spam):
    pass


m = Other()
m.printNumInstances()

a = Spam()
b = Spam()
c = Spam()

# Spam.printNumInstances()
# a.printNumInstances()
m.printNumInstances()

x = Sub()
y = Sub()
# x.printNumInstances()
# Sub.printNumInstances()
# Spam.printNumInstances()
m.printNumInstances()
