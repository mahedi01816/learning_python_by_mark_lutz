class Newprops(object):
    def getage(self):
        return 40

    def setage(self, value):
        print('Set Age : ', value)
        self._age = value

    age = property(getage, setage, None, None)


x = Newprops()
print(x.age)

x.age = 42
print(x._age)

x.job = 'trainer'
print(x.job)
