def printNumInstances():
    print('Number of instances created : ', Spam.numInstances)


class Spam:
    numInstances = 0

    def __init__(self):
        Spam.numInstances = Spam.numInstances + 1


a = Spam()
b = Spam()
c = Spam()

printNumInstances()
print(Spam.numInstances)
