def minmax(test, *args):
    res = args[0]
    for arg in args[1:]:
        if test(arg, res):
            res = arg
    return res


def lessThan(x, y):
    return x < y


def greaterThan(x, y):
    return x > y


print(minmax(lessThan, 4, 2, 1, 5, 6, 3))
print(minmax(greaterThan, 4, 2, 1, 5, 6, 3))
