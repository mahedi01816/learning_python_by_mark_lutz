def intersect(*args):
    res = []
    for x in args[0]:
        for other in args[1:]:
            if x not in other:
                break
        else:
            res.append(x)
    return res


def union(*args):
    res = []
    for seq in args:
        for x in seq:
            if x not in res:
                res.append(x)
    return res


s1 = "SPAM"
s2 = "SCAM"
s3 = "SLAM"
print(intersect(s1, s2))
print(union(s2, s3))
print(intersect([1, 2, 3], (1, 4)))
print(intersect(s1, s2, s3))
print(union(s1, s2, s3))
