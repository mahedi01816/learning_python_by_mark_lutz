"""
def min1(*args):
    res = args[0]
    for arg in args[1:]:
        if arg < res:
            res = arg
    return res


print(min1(3, 4, 2, 1))
"""

"""
def min2(first, *rest):
    for arg in rest:
        if arg < first:
            first = arg
    return first


print(min2(3, 4, 2, 1))
"""


def min3(*args):
    tmp = list(args)
    tmp.sort()
    return tmp[0]


print(min3(3, 4, 2, 1))
