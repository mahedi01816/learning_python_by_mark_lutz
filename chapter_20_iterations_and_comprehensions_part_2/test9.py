def myzip(*seqs):
    minlen = min(len(S) for S in seqs)
    return (tuple(S[i] for S in seqs) for i in range(minlen))


S1 = 'abc'
S2 = 'xya123'
print(list(myzip(S1, S2)))
