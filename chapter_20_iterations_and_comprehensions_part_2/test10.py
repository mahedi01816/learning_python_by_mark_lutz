def myzip(*args):
    # iters = map(iter, args)
    iters = list(map(iter, args))
    while iters:
        res = [next(i) for i in iters]
        yield tuple(res)


print(list(myzip('abc', 'lmnop')))
