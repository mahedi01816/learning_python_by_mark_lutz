def gen():
    for i in range(10):
        X = yield i
        print(X)


G = gen()
print(next(G))
print(G.send(77))
print(G.send(88))
print(next(G))
