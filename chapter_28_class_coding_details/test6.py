X = 11


def g1():
    print(X)


def g2():
    global X
    X = 22


def h1():
    X = 33

    def nested():
        print(X)


def h2():
    X = 33

    def nested():
        nonlocal X
        X = 44


if __name__ == '__main__':
    print(X)
    g1()
    g2()
    print(X)
    h1()
    print(X)
    h2()
    print(X)
