class Super:
    def delegate(self):
        self.action()

    def action(self):
        raise NotImplementedError('action must be defined!')
        # assert False, 'action must be defined!'


class Sub(Super):
    def action(self):
        print('spam')


X = Sub()
X.delegate()
