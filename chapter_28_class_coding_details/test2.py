class NextClass:
    def printer(self, text):
        self.message = text
        print(self.message)


x = NextClass()
x.printer('instance call')
print(x.message)

NextClass.printer(x, 'class call')
print(x.message)


# TypeError: printer() missing 1 required positional argument: 'text'
# NextClass.printer('bad call')
