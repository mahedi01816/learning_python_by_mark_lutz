class D:
    def __get__(*args):
        print('get')

    def __set__(*args):
        raise AttributeError('can not set')


class C:
    a = D()


x = C()
x.a
x.a = 99