class Descriptor(object):
    def __get__(self, instance, owner):
        print(self, instance, owner, sep=' ')


class Subject:
    attr = Descriptor()


x = Subject()
x.attr
Subject.attr
