class PropSquare:
    def __init__(self, start):
        self.value = start

    def getX(self):
        return self.value ** 2

    def setX(self, value):
        self.value = value

    X = property(getX, setX)


p = PropSquare(3)
q = PropSquare(32)

print(p.X)
p.X = 4
print(p.X)
print(q.X)
