class Person:
    def __init__(self, name):
        self._name = name

    """
    def __getattr__(self, item):
        if item == 'name':
            print('fetch...')
            return self._name
        else:
            raise AttributeError(item)
    """

    def __getattribute__(self, item):
        if item == 'name':
            print('fetch...')
            item = '_name'
        return object.__getattribute__(self, item)

    def __setattr__(self, key, value):
        if key == 'name':
            print('change...')
            key = '_name'
        self.__dict__[key] = value

    def __delattr__(self, item):
        if item == 'name':
            print('remove...')
            item = '_name'
            del self.__dict__[item]


bob = Person('Bob Smith')
print(bob.name)
bob.name = 'Robert Smith'
print(bob.name)
del bob.name

print('-' * 20)
sue = Person('Sue Jones')
print(sue.name)
