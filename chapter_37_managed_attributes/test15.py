class AttrSquare:
    def __init__(self, start):
        self.value = start

    def __getattr__(self, item):
        if item == 'X':
            return self.value ** 2
        else:
            raise AttributeError(item)

    def __setattr__(self, key, value):
        if key == 'X':
            key = 'value'
        self.__dict__[key] = value


a = AttrSquare(3)
b = AttrSquare(32)

print(a.X)
a.X = 4
print(a.X)
print(b.X)
