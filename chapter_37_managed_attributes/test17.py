class GetAttr:
    attr1 = 1

    def __init__(self):
        self.attr2 = 2

    def __getattr__(self, item):
        print('get : ' + item)
        return 3


x = GetAttr()
print(x.attr1)
print(x.attr2)
print(x.attr3)
print('-' * 40)


class GetAttribute(object):
    attr1 = 1

    def __init__(self):
        self.attr2 = 2

    def __getattribute__(self, item):
        print('get : ' + item)
        if item == 'attr3':
            return 3
        else:
            return object.__getattribute__(self, item)


x = GetAttribute()
print(x.attr1)
print(x.attr2)
print(x.attr3)
