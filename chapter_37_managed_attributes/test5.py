class D:
    def __get__(*args):
        print('get')


class C:
    a = D()


x = C()
x.a
C.a
x.a = 99
print(x.a)
print(list(x.__dict__.keys()))
y = C()
y.a
C.a
