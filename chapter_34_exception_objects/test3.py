class E(Exception):
    pass


try:
    raise E('spam', 'eggs', 'ham')
except E as X:
    print(X, X.args)
