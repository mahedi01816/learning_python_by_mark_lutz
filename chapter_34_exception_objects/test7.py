class FormatError(Exception):
    pass


def parser():
    raise FormatError(42, 'spam.txt')


try:
    parser()
except FormatError as X:
    print('Error at ', X.args[0], X.args[1])
