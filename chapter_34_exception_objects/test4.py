class MyBad(Exception):
    pass


try:
    raise MyBad('sorry -- my mistake!!!')
except MyBad as X:
    print(X)

raise MyBad('sorry')
