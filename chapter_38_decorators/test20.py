traceMe = False


def trace(*args):
    if traceMe:
        print('[' + ' '.join(map(str, args)) + ']')


def accessControl(failIf):
    def onDecorator(aClass):
        if not __debug__:
            return aClass
        else:
            class onInstance:
                def __init__(self, *args, **kwargs):
                    self.__wrapped = aClass(*args, **kwargs)

                def __getattr__(self, item):
                    trace('get : ', item)
                    if failIf(item):
                        raise TypeError('private attribute fetch : ', item)
                    else:
                        return getattr(self.__wrapped, item)

                def __setattr__(self, key, value):
                    trace('set : ', key, value)
                    if key == '_onInstance__wrapped':
                        self.__dict__[key] = value
                    elif failIf(key):
                        raise TypeError('private attribute change : ', key)
                    else:
                        setattr(self.__wrapped, key, value)

            return onInstance

    return onDecorator


def private(*attributes):
    return accessControl(failIf=(lambda attr: attr in attributes))


def public(*attributes):
    return accessControl(failIf=(lambda attr: attr not in attributes))


@private('age')
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


X = Person('Bob', 40)
print(X.name)
X.name = 'Sue'
print(X.name)


@public('name')
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


X = Person('Bob', 40)
print(X.name)
X.name = 'Sue'
print(X.name)
