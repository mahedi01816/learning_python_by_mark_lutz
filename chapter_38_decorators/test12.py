class Tracer:
    def __init__(self, aClass):
        self.aClass = aClass

    def __call__(self, *args):
        self.wrapped = self.aClass(*args)
        return self

    def __getattr__(self, item):
        print('Trace : ' + item)
        return getattr(self.wrapped, item)


@Tracer
class Spam:
    def display(self):
        print('Spam !' * 8)


food = Spam()
food.display()
