def rangeTest(**argchecks):
    def onDecorator(func):
        def onCall(*pargs, **kwargs):
            print(argchecks)
            for check in argchecks:
                pass
            return func(*pargs, **kwargs)

        return onCall

    return onDecorator


@rangeTest(a=(1, 5), c=(0.0, 1.0))
def func(a, b, c):
    print(a + b + c)


func(1, 2, c=3)
