traceMe = False


def trace(*args):
    if traceMe:
        print('[' + ' '.join(map(str, args)) + ']')


def accessControl(failIf):
    def onDecorator(aClass):
        class OnInstance:
            def __init__(self, *args, **kwargs):
                self.__wrapped = aClass(*args, **kwargs)

            def __getattr__(self, item):
                trace('get : ', item)
                if failIf(item):
                    raise TypeError('private attributr fetch : ' + item)
                else:
                    return getattr(self.__wrapped, item)

            def __setattr__(self, key, value):
                trace('set : ', key, value)
                if key == '_OnInstance__wrapped':
                    self.__dict__[key] = value
                elif failIf(key):
                    raise TypeError('private attribute change : ' + key)
                else:
                    setattr(self.__wrapped, key, value)

        return OnInstance

    return onDecorator


def private(*attributes):
    return accessControl(failIf=(lambda attr: attr in attributes))


def public(*attributes):
    return accessControl(failIf=(lambda attr: attr not in attributes))


@private('age')
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


x = Person('Bob', 40)
print(x.name)

x.name = 'Sue'
print(x.name)
# print(x.age)
# x.age = 'Tom'
