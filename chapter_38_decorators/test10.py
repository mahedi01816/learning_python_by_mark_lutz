class Wrapper:
    def __init__(self, object):
        self.wrapped = object

    def __getattr__(self, item):
        print('Trace : ', item)
        return getattr(self.wrapped, item)


x = Wrapper([1, 2, 3])
x.append(4)
print(x.wrapped)

x = Wrapper({'a': 2, 'b': 3})
print(list(x.keys()))
