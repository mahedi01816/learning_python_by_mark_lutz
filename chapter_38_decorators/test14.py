def decorate(func):
    func.marked = True
    return func


@decorate
def spam(a, b):
    return a + b


print(spam.marked)


def annotate(text):
    def decorate(func):
        func.label = text
        return func

    return decorate


@annotate('spam data')
def spam(a, b):
    return a + b


print(spam(1, 2), spam.label)
