from devtools1 import rangeTest


@rangeTest(age=(0, 120))
def persinfo(name, age):
    print('%s is %s years old' % (name, age))


@rangeTest(M=(1, 12), D=(1, 31), Y=(0, 2016))
def birthday(M, D, Y):
    print('birthday = {0}/{1}/{2}'.format(M, D, Y))


persinfo('Bob', 40)
persinfo(age=40, name='Bob')
birthday(5, D=1, Y=1963)


class Person:
    def __init__(self, name, job, pay):
        self.job = job
        self.pay = pay

    @rangeTest(percent=(0.0, 1.0))
    def giveRaise(self, percent):
        self.pay = int(self.pay * (1 + percent))


bob = Person('Bob Smith', 'dev', 10000)
sue = Person('Sue Smith', 'dev', 10000)
bob.giveRaise(.10)
sue.giveRaise(percent=.20)
print(bob.pay, sue.pay)
