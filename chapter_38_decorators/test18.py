def rangeTest(func):
    def onCall(*pargs, **kwargs):
        argchecks = func.__annotations__
        print(argchecks)
        for check in argchecks:
            pass
        return func(*pargs, **kwargs)

    return onCall


@rangeTest
def func(a: (1, 5), b, c: (0.0, 1.0)):
    print(a + b + c)


func(1, 2, c=3)
