def intersect(seq1, seq2):
    res = []
    for x in seq1:
        if x in seq2:
            res.append(x)
    return res


print(intersect([1, 2, 3], (1, 4)))
seq1 = "SPAM"
seq2 = "SCAM"
print(intersect(seq1, seq2))
"""
seq1 = [1, 2, 3, 4]
seq2 = [4, 5, 6, 7]
print(intersect(seq1, seq2))
"""
# print(intersect([0, 1, 1, 2, 3, 4, 5, 6, 7, 8], [6, 5, 4, 3, 2]))
