def func():
    x = 4
    action = (lambda n: x ** n)
    return action


x = func()
print(x(2))
