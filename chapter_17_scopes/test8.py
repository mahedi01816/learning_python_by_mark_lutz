def func():
    x = 4
    action = (lambda n, x=x: x ** n)
    return action


test = func()
print(test(2))
