"""
====================================================
# SyntaxError: no binding for nonlocal 'spam' found
====================================================
spam = 99
def tester():
    def nested():
        nonlocal spam
        print('Current = ', spam)
        spam += 1
    return nested
"""
