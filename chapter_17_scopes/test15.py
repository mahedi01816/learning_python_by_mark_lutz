class Tester:
    def __init__(self, start):
        self.state = start

    def nested(self, label):
        print(label, self.state)
        self.state += 1


F = Tester(0)
F.nested('spam')
F.nested('ham')
G = Tester(42)
G.nested('hi')
F.nested('hello')
