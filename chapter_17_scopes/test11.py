"""
=====================================================
# SyntaxError: no binding for nonlocal 'state' found
=====================================================
def tester(start):
    def nested(label):
        nonlocal state
        state = 0
        print(label, state)

    return nested
"""


def tester(start):
    def nested(label):
        global state
        state = 0
        print(label, state)

    return nested


F = tester(0)
F('abc')
print(state)
