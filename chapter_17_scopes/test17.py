def tester(start):
    def nested(label):
        print(label, nested.state)
        nested.state += 1

    nested.state = start
    return nested


F = tester(0)
F('spam')
F('hi')
print(F.state)
G = tester(55)
G('hello')
F('what')
print(G.state)