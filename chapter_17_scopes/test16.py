class Tester:
    def __init__(self, start):
        self.state = start

    def __call__(self, label):
        print(label, self.state)
        self.state += 1


H = Tester(99)
H('juice')
H('hi')
