def tester(start):
    global state
    state = start

    def nested(label):
        global state
        print(label, state)
        state += 1

    return nested


F = tester(0)
F('spam')
F('eggs')
G = tester(42)
G('ham')
F('ham')  # problem
