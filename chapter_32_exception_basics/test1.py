def fetcher(obj, index):
    return obj[index]


x = 'spam'
"""
print(fetcher(x, 3))
try:
    fetcher(x, 4)
except IndexError:
    print('got exception')


def catcher():
    try:
        fetcher(x, 4)
    except IndexError:
        print('got exception')
    print('continuing')


catcher()

try:
    raise IndexError
except IndexError:
    print('got exception!')

try:
    fetcher(x, 3)
finally:
    print('after fetch')
"""


def after():
    try:
        fetcher(x, 3)
    finally:
        print('after fetch')
    print('after try?')


after()
