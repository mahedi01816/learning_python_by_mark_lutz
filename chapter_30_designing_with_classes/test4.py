class Selfless:
    def __init__(self, data):
        self.data = data

    def selfless(arg1, arg2):
        return arg1 + arg2

    def normal(self, arg1, arg2):
        return self.data + arg1 + arg2


X = Selfless(2)
print(X.normal(3, 4))

print(Selfless.normal(X, 3, 4))
print(Selfless.selfless(3, 4))

# TypeError: selfless() takes 2 positional arguments but 3 were given
# print(X.selfless(3,4))

# TypeError: normal() missing 1 required positional argument: 'arg2'
# print(Selfless.normal(3,4))
