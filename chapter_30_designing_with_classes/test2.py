class Spam:
    def doit(self, message):
        print(message)


object1 = Spam()

t = Spam.doit
t(object1, 'Hi!!!')

x = object1.doit
x('Hello there!')

object1.doit('Hello world!')
