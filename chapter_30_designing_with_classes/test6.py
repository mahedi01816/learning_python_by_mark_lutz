def square(arg):
    return arg ** 2


class Sum:
    def __init__(self, value):
        self.value = value

    def __call__(self, arg):
        return self.value + arg


class Product:
    def __init__(self, value):
        self.value = value

    def method(self, arg):
        return self.value * arg


class Negate:
    def __init__(self, value):
        self.value = - value

    def __repr__(self):
        return str(self.value)


sobject = Sum(2)
pobject = Product(3)

actions = [square, sobject, pobject.method, Negate]
for act in actions:
    print(act(5))

print(actions[-1](5))
print([act(5) for act in actions])
print(list(map(lambda act: act(5), actions)))

table = {act(5): act for act in actions}
for (key, value) in table.items():
    print('{0:2} => {1}'.format(key, value))
