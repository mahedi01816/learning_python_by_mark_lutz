from streams import Processor


class UpperCase(Processor):
    def converter(self, data):
        return data.upper()


if __name__ == '__main__':
    import sys

    obj = UpperCase(open('spam'), sys.stdout)  # (spam == spam.txt)
    obj.process()
