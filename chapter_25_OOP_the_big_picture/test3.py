def processor(reader, converter, writer):
    while 1:
        data = reader.read()
        if not data:
            break
        data = converter(data)
        writer.write(data)


class Reader:
    def read(self):
        pass

    def other(self):
        pass


class FileReader(Reader):
    def read(self):
        pass


class SocketReader(Reader):
    def read(self):
        pass

# processor(FileReader(...), Converter, FileWriter(...))
