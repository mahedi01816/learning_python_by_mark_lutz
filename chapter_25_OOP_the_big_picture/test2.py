class Employee:
    def computeSalary(self):
        pass

    def giveRaise(self):
        pass

    def promote(self):
        pass

    def retire(self):
        pass


class Engineer(Employee):
    def computeSalary(self):
        pass


bob = Employee()
mel = Engineer()

company = [bob, mel]
for emp in company:
    print(emp.computeSalary())